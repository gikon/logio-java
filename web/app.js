/**
 * Created by giko on 12/3/14.
 */
app = angular.module('YourApp', ['ngMaterial', 'hmTouchEvents']);
app.filter('reverse', function() {
    return function(items) {
        return items.slice().reverse();
    };
});

app.factory('socket', function ($rootScope) {
    var socket = io.connect('http://localhost:9191/');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
});

app.factory('logService', function (socket) {
    var messages = [];
    var streams = [];
    socket.on('log', function (data) {
        console.log(data);
        messages = messages.concat(data);
    });

    socket.on('stream', function (data) {
        console.log(data);
        streams = streams.concat(data.name);
    });

    return {
        getMessages: function () {
            return messages;
        },
        getStreams: function () {
            return streams;
        }
    };
});

app.controller('ToolbarCtrl', function ($scope) {
});

app.controller('LeftCtrl', function ($scope) {
    $scope.close = function () {
        $mdSidenav('left').close();
    };
});

app.controller('AppCtrl', function ($scope, $mdToast, $mdSidenav, logService) {
    $scope.toggleSideNav = function () {
        $mdSidenav('left').toggle();
    };

    $scope.getMessages = logService.getMessages;
    $scope.getColorForStream = function(stream) {
        return "color"+logService.getStreams().indexOf(stream);
    };

    $scope.showSimpleToast = function () {
        $mdToast.show({
            position: "bottom right",
            template: "<md-toast>Toast !</md-toast>"
        });
    };
});