package org.kluge.logging;

import io.reactivex.netty.RxNetty;
import io.reactivex.netty.pipeline.PipelineConfigurators;
import io.reactivex.netty.server.RxServer;
import org.kluge.logging.model.LogServerEvent;
import org.kluge.logging.model.LogServerState;
import org.kluge.logging.model.LogStream;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by giko on 10/12/2014.
 */
public class LogServer {
    private RxServer<String, String> server;
    private LogServerState state = new LogServerState();
    private LinkedList<LogServerEvent> eventCache = new LinkedList<>();

    public LogServer() {
        server = RxNetty.createTcpServer(28777, PipelineConfigurators.stringMessageConfigurator(), new LogServerConnectionHandler(state));
        state.getCachingEventSubject().subscribe(new Subscriber<LogServerEvent>() {
            @Override public void onCompleted() {
                
            }

            @Override public void onError(Throwable throwable) {

            }

            @Override public void onNext(LogServerEvent logServerEvent) {
                if (eventCache.size() >= 50){
                    eventCache.addLast(logServerEvent);
                    eventCache.removeFirst();
                }
                else
                {
                    eventCache.add(logServerEvent);
                }
            }
        });
    }

    private Observable<LogServerEvent> generateWelcomingEvents() {
        List<LogServerEvent> events = new LinkedList<>();
        for (LogStream stream : state.getStreams()) {
            events.add(new LogServerEvent("stream", stream));
        }
        
        events.addAll(eventCache);

        return Observable.from(events);
    }

    public void subscribe(Observer<LogServerEvent> observer) {
        generateWelcomingEvents().mergeWith(state.getObservable()).subscribe(observer);
    }

    public void start() {
        server.start();
    }
    
    public void stop(){
        try {
            server.shutdown();
        } catch (InterruptedException e) {
            throw new RuntimeException("An error occurred!", e);
        }
    }
}
