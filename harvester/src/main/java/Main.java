import java.io.*;
import java.net.Socket;
import java.nio.file.*;
import java.util.*;

/**
 * Created by giko on 10/13/14.
 */
public class Main {
    private static String sysDelimeter = System.getProperty("line.separator");

    public static void main(String[] args) {
        try {
            Socket clientSocket = new Socket(System.getenv().get("logioHost"), 28777);
            OutputStreamWriter outToServer = new OutputStreamWriter(clientSocket.getOutputStream());

            String nodeName = System.getenv().get("vName");
            Scanner s = null;
            try {
                s = new Scanner(new File(args[0]));
            } catch (FileNotFoundException e) {
                throw new RuntimeException("We are fucked, file not found!", e);
            }
            s.useDelimiter(sysDelimeter);
            List<String> globs = new ArrayList<String>();
            while (s.hasNext()) {
                globs.add(s.next());
            }
            s.close();
            Set<String> streamNames = new HashSet<>();
            for (String glob : globs) {

                String[] argsL = glob.split("\\ ");
                streamNames.add(argsL[0]);
                Path directory = Paths.get(argsL[1]);
                WatchService watchService = directory.getFileSystem().newWatchService();
                MyWatchQueueReader reader = new MyWatchQueueReader(watchService,
                        FileSystems.getDefault().getPathMatcher("glob:".concat(argsL[2])), argsL[1], outToServer,
                        argsL[0], nodeName);
                Thread readerThread = new Thread(reader);
                readerThread.start();
                directory.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
            }

            String streamNamesString = "";
            Iterator<String> iterator = streamNames.iterator();
            int i = 0;
            while (iterator.hasNext()) {
                if (i != 0) {
                    streamNamesString = streamNamesString.concat("," + iterator.next());
                } else {
                    streamNamesString = iterator.next();
                }
                ++i;
            }
            outToServer.write("+node|".concat(nodeName) + "|" + streamNamesString + "\r\n");
            outToServer.write("+bind|node|".concat(nodeName) + "\r\n");
            outToServer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class MyWatchQueueReader implements Runnable {
        private WatchService myWatcher;
        private PathMatcher matcher;
        private String path;
        private Map<String, Long> fileSizes = new HashMap<>();
        private OutputStreamWriter outputStream;
        private String stream;
        private String nodeName;

        public MyWatchQueueReader(WatchService myWatcher, PathMatcher matcher, String path,
                OutputStreamWriter dataOutputStream, String stream, String nodeName) {
            this.myWatcher = myWatcher;
            this.matcher = matcher;
            this.path = path;
            this.outputStream = dataOutputStream;
            this.stream = stream;
            this.nodeName = nodeName;
        }

        @Override
        public void run() {
            try {
                WatchKey key = myWatcher.take();
                while (key != null) {
                    for (WatchEvent event : key.pollEvents()) {
                        if (matcher.matches(Paths.get(event.context().toString()))) {
                            File file = Paths.get(path + "/" + event.context().toString()).toFile();
                            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
                            long offset = fileSizes.get(event.context().toString()) == null ?
                                    0 :
                                    fileSizes.get(event.context().toString());
                            long len = (randomAccessFile.length() - offset);
                            byte[] changes = new byte[(int) len];
                            randomAccessFile.seek(offset);
                            randomAccessFile.readFully(changes);
                            fileSizes.put(event.context().toString(), randomAccessFile.length());
                            randomAccessFile.close();
                            String[] lines = new String(changes).split(sysDelimeter);
                            for (String line : lines) {
                                if (!line.equals("")) {
                                    outputStream
                                            .write("+log|" + stream + "|" + nodeName + "|info|" + line + "\r\n");
                                }
                            }
                            outputStream.flush();
                        }
                    }
                    key.reset();
                    key = myWatcher.take();
                }
            } catch (InterruptedException | IOException e) {
                throw new RuntimeException("An error occurred!", e);
            }
            System.out.println("Stopping thread");
        }
    }
}
